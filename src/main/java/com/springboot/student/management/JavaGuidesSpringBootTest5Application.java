package com.springboot.student.management;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.springboot.student.management.entity.Student;
import com.springboot.student.management.repository.StudnetRepository;

@SpringBootApplication
public class JavaGuidesSpringBootTest5Application implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(JavaGuidesSpringBootTest5Application.class, args);
	}

	@Autowired
	private StudnetRepository studnetRepository;

	@Override
	public void run(String... args) throws Exception {
//		Student student1 = new Student((long) 0, "Dhanuka", "Madhusanka", "dhanuka@gmail.com");
//		studnetRepository.save(student1);
//
//		Student student2 = new Student((long) 0, "Dhanuka2", "Madhusanka2", "dhanuka2@gmail.com");
//		studnetRepository.save(student2);
//
//		Student student3 = new Student((long) 0, "Dhanuka3", "Madhusanka3", "dhanuka3@gmail.com");
//		studnetRepository.save(student3);
	}
}
