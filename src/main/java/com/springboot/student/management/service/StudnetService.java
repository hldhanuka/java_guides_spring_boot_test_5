package com.springboot.student.management.service;

import java.util.List;

import com.springboot.student.management.entity.Student;

public interface StudnetService {
	List<Student> getAllStudents();

	Student saveStudent(Student student);

	Student getStudentById(Long id);

	Student updateStudent(Student student);

	void deleteStudentById(Long id);
}
