package com.springboot.student.management.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.springboot.student.management.entity.Student;
import com.springboot.student.management.repository.StudnetRepository;
import com.springboot.student.management.service.StudnetService;

@Service
public class StudentServiceImpl implements StudnetService{
	private StudnetRepository studnetRepository;

	public StudentServiceImpl(StudnetRepository studnetRepository) {
		super();
		this.studnetRepository = studnetRepository;
	}

	@Override
	public List<Student> getAllStudents() {
		return studnetRepository.findAll();
	}

	@Override
	public Student saveStudent(Student student) {
		return studnetRepository.save(student);
	}

	@Override
	public Student getStudentById(Long id) {
		return studnetRepository.findById(id).get();
	}

	@Override
	public Student updateStudent(Student student) {
		return studnetRepository.save(student);
	}

	@Override
	public void deleteStudentById(Long id) {
		studnetRepository.deleteById(id);
	}
}
