package com.springboot.student.management.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.student.management.entity.Student;

public interface StudnetRepository extends JpaRepository<Student, Long>{
	
}