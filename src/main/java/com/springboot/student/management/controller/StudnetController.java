package com.springboot.student.management.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.springboot.student.management.entity.Student;
import com.springboot.student.management.service.StudnetService;

@Controller
public class StudnetController {
	private StudnetService studnetService;

	public StudnetController(StudnetService studnetService) {
		super();
		this.studnetService = studnetService;
	}

	// handler method to handle list students and returns mode and view
	@GetMapping("/students")
	public String listStudents(Model model) {
		model.addAttribute("students", studnetService.getAllStudents());

		return "students";
	}

	@PostMapping("/students")
	public String saveStudent(@ModelAttribute("student") Student student) {
		studnetService.saveStudent(student);

		return "redirect:/students";
	}

	@GetMapping("/students/new")
	public String createStudentForm(Model model) {
		Student student = new Student();

		model.addAttribute("student", student);

		return "create_student";
	}

	@GetMapping("/students/edit/{id}")
	public String editStudentForm(@PathVariable Long id ,Model model) {
		model.addAttribute("student", studnetService.getStudentById(id));

		return "edit_student";
	}

	@PostMapping("/students/{id}")
	public String updateStudent(
		@PathVariable Long id,
		@ModelAttribute("student") Student student,
		Model model
	) {
		Student existingStudent = studnetService.getStudentById(id);

		existingStudent.setFirstName(student.getFirstName());
		existingStudent.setLastName(student.getLastName());
		existingStudent.setEmail(student.getEmail());

		studnetService.updateStudent(existingStudent);

		return "redirect:/students";
	}

	@GetMapping("/students/{id}")
	public String deleteStudentForm(@PathVariable Long id) {
		studnetService.deleteStudentById(id);

		return "redirect:/students";
	}

}
